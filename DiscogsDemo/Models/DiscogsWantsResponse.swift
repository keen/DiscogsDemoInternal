//
//  DiscogsAlbums.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/9/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import Foundation

struct DiscogsWantsResponse: Codable {
    
    // Currently, this model is ignoring the pagination data for simplicity of UI.
    //    The API will automatically return the first 50 results (wanted albums).
    
    var wantedItems: [DiscogsAlbumSummary]?
    var pagination: Pagination?
    
    enum CodingKeys: String, CodingKey {
        case wantedItems    = "wants"
        case pagination
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        wantedItems = try values.decode([DiscogsAlbumSummary].self, forKey: .wantedItems)
        pagination  = try values.decodeIfPresent(Pagination.self, forKey: .pagination)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(wantedItems, forKey: .wantedItems)
        try container.encodeIfPresent(pagination, forKey: .pagination)
    }
}

struct DiscogsAlbumSummary: Codable {
    var rating: Int?
    var resource_url: String?
    var basicInformation: DiscogsAlbumBasicInformation?
    
    enum CodingKeys: String, CodingKey {
        case rating
        case resource_url
        case basicInformation   = "basic_information"
    }
}

struct DiscogsAlbumBasicInformation: Codable {
    var artists: [Artist]?
    var recordLabels: [RecordLabel]?
    let title: String?
    let coverImage: String?
    let year: Int?
    let id: Int?
    
    enum CodingKeys: String, CodingKey {
        case artists
        case recordLabels       = "labels"
        case coverImage         = "cover_image"
        case title
        case year
        case id
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        artists = try values.decodeIfPresent([Artist].self, forKey: .artists)
        recordLabels = try values.decodeIfPresent([RecordLabel].self, forKey: .recordLabels)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        coverImage = try values.decodeIfPresent(String.self, forKey: .coverImage)
        year = try values.decodeIfPresent(Int.self, forKey: .year)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(artists, forKey: .artists)
        try container.encodeIfPresent(recordLabels, forKey: .recordLabels)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encodeIfPresent(coverImage, forKey: .coverImage)
        try container.encodeIfPresent(year, forKey: .year)
        try container.encodeIfPresent(id, forKey: .id)
    }
}

struct RecordLabel: Codable {
    let id: Int?
    let name: String?
    let entityType: String?
    let catalogNumber: String?
    let resourceURL: String?
    let entityTypeName: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case entityType     = "entity_type"
        case catalogNumber  = "catno"
        case resourceURL    = "resource_url"
        case entityTypeName = "entity_type_name"
    }
}

struct Artist: Codable {
    let id: Int?
    let name: String?
    let role: String?
    let resourceURL: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case role
        case resourceURL    = "resource_url"
    }
}

struct Pagination: Codable {
    let page: Int?
    let urls: PaginationLinks?
    
    enum CodingKeys: String, CodingKey {
        case page
        case urls
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        page = try values.decodeIfPresent(Int.self, forKey: .page)
        urls = try values.decodeIfPresent(PaginationLinks.self, forKey: .urls)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(page, forKey: .page)
        try container.encodeIfPresent(urls, forKey: .urls)
    }
}

struct PaginationLinks: Codable {
    let first: String?
    let prev: String?
    let next: String?
    let last: String?
    
    enum CodingKeys: String, CodingKey {
        case first
        case prev
        case next
        case last
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        first = try values.decodeIfPresent(String.self, forKey: .first)
        prev  = try values.decodeIfPresent(String.self, forKey: .prev)
        next  = try values.decodeIfPresent(String.self, forKey: .next)
        last  = try values.decodeIfPresent(String.self, forKey: .last)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(first, forKey: .first)
        try container.encodeIfPresent(prev, forKey: .prev)
        try container.encodeIfPresent(next, forKey: .next)
        try container.encodeIfPresent(last, forKey: .last)
    }
}
