//
//  DiscogsAlbumResource.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/10/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import Foundation

struct DiscogsAlbumResource: Codable {
    let id: Int?
    let title: String?
    let year: Int?
    let community: Community?
    let artists: [Artist]?
    let images: [DiscogsImage]?
    let genres: [String]?
    let released: String?
    let dateAdded: String?
    let tracklist: [Track]?
    let country: String?
    let notes: String?
    let styles: [String]?
    let releasedFormatted: String?
    let identifiers: [Identifier]?
    let formats: [Format]?
    let uri: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case title
        case year
        case community
        case artists            = "extraartists"
        case images
        case genres
        case released
        case dateAdded          = "date_added"
        case tracklist
        case country
        case notes
        case styles
        case releasedFormatted  = "released_formatted"
        case identifiers
        case formats
        case uri
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id                  = try values.decodeIfPresent(Int.self, forKey: .id)
        title               = try values.decodeIfPresent(String.self, forKey: .title)
        year                = try values.decodeIfPresent(Int.self, forKey: .year)
        community           = try values.decodeIfPresent(Community.self, forKey: .community)
        artists             = try values.decodeIfPresent([Artist].self, forKey: .artists)
        images              = try values.decodeIfPresent([DiscogsImage].self, forKey: .images)
        genres              = try values.decodeIfPresent([String].self, forKey: .genres)
        styles              = try values.decodeIfPresent([String].self, forKey: .styles)
        released            = try values.decodeIfPresent(String.self, forKey: .released)
        dateAdded           = try values.decodeIfPresent(String.self, forKey: .dateAdded)
        tracklist           = try values.decodeIfPresent([Track].self, forKey: .tracklist)
        country             = try values.decodeIfPresent(String.self, forKey: .country)
        notes               = try values.decodeIfPresent(String.self, forKey: .notes)
        releasedFormatted   = try values.decodeIfPresent(String.self, forKey: .releasedFormatted)
        identifiers         = try values.decodeIfPresent([Identifier].self, forKey: .identifiers)
        formats             = try values.decodeIfPresent([Format].self, forKey: .formats)
        uri                 = try values.decodeIfPresent(String.self, forKey: .uri)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encodeIfPresent(id, forKey: .id)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encodeIfPresent(year, forKey: .year)
        try container.encodeIfPresent(community, forKey: .community)
        try container.encodeIfPresent(artists, forKey: .artists)
        try container.encodeIfPresent(images, forKey: .images)
        try container.encodeIfPresent(genres, forKey: .genres)
        try container.encodeIfPresent(styles, forKey: .styles)
        try container.encodeIfPresent(released, forKey: .released)
        try container.encodeIfPresent(dateAdded, forKey: .dateAdded)
        try container.encodeIfPresent(tracklist, forKey: .tracklist)
        try container.encodeIfPresent(country, forKey: .country)
        try container.encodeIfPresent(notes, forKey: .notes)
        try container.encodeIfPresent(releasedFormatted, forKey: .releasedFormatted)
        try container.encodeIfPresent(identifiers, forKey: .identifiers)
    }
}

struct Community: Codable {
    let status: String?
    let rating: Rating?
    let have: Int?
    let want: Int?
    let dataQuality: String?
 
    enum CodingKeys: String, CodingKey {
        case status
        case rating
        case have
        case want
        case dataQuality = "data_quality"
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        status      = try values.decodeIfPresent(String.self, forKey: .status)
        rating      = try values.decodeIfPresent(Rating.self, forKey: .rating)
        have        = try values.decodeIfPresent(Int.self, forKey: .have)
        want        = try values.decodeIfPresent(Int.self, forKey: .want)
        dataQuality = try values.decodeIfPresent(String.self, forKey: .dataQuality)
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(status, forKey: .status)
        try container.encodeIfPresent(rating, forKey: .rating)
        try container.encodeIfPresent(have, forKey: .have)
        try container.encodeIfPresent(want, forKey: .want)
        try container.encodeIfPresent(dataQuality, forKey: .dataQuality)
    }
}

struct Track: Codable {
    let duration: String?
    let position: String?
    let title: String?
    let type: String?
    let subtracks: [Track]?
    
    enum CodingKeys: String, CodingKey {
        case duration
        case position
        case title
        case type       = "type_"
        case subtracks  = "sub_tracks"
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        duration  = try values.decodeIfPresent(String.self, forKey: .duration)
        position  = try values.decodeIfPresent(String.self, forKey: .position)
        title     = try values.decodeIfPresent(String.self, forKey: .title)
        type      = try values.decodeIfPresent(String.self, forKey: .type)
        subtracks = try values.decodeIfPresent([Track].self, forKey: .subtracks)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(duration, forKey: .duration)
        try container.encodeIfPresent(position, forKey: .position)
        try container.encodeIfPresent(title, forKey: .title)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(subtracks, forKey: .subtracks)
    }
}

struct DiscogsImage: Codable {
    let uri: String?
    let height: Int?
    let width: Int?
    let resourceURL: String?
    let type: String?
    let uri150: String?
    
    enum CodingKeys: String, CodingKey {
        case uri
        case height
        case width
        case resourceURL = "resource_url"
        case type
        case uri150
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        uri         = try values.decodeIfPresent(String.self, forKey: .uri)
        height      = try values.decodeIfPresent(Int.self, forKey: .height)
        width       = try values.decodeIfPresent(Int.self, forKey: .width)
        resourceURL = try values.decodeIfPresent(String.self, forKey: .resourceURL)
        type        = try values.decodeIfPresent(String.self, forKey: .type)
        uri150      = try values.decodeIfPresent(String.self, forKey: .uri150)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(uri, forKey: .uri)
        try container.encodeIfPresent(height, forKey: .height)
        try container.encodeIfPresent(width, forKey: .width)
        try container.encodeIfPresent(resourceURL, forKey: .resourceURL)
        try container.encodeIfPresent(type, forKey: .type)
        try container.encodeIfPresent(uri150, forKey: .uri150)
    }
}

struct Identifier: Codable {
    let type: String?
    let value: String?
}

struct Rating: Codable {
    let count: Int?
    let average: Float?
}

struct Format: Codable, Equatable {
    let descriptions: [String]?
    let name: String?
    let qty: String?
    
    enum CodingKeys: String, CodingKey {
        case descriptions
        case name
        case qty
    }
    
    // Custom initializer to use in testing for the custom equality comparator.
    init(withDescriptions descriptions:[String]?, quantity: String?, andName name: String?) {
        
        self.descriptions = descriptions
        self.name = name
        self.qty = quantity
    }
    
    // Boilerplate encode/decode
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        descriptions    = try values.decodeIfPresent([String].self, forKey: .descriptions)
        name            = try values.decodeIfPresent(String.self, forKey: .name)
        qty             = try values.decodeIfPresent(String.self, forKey: .qty)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encodeIfPresent(descriptions, forKey: .descriptions)
        try container.encodeIfPresent(name, forKey: .name)
        try container.encodeIfPresent(qty, forKey: .qty)
    }
    
    static func == (lhs: Format, rhs: Format) -> Bool {
        return (lhs.descriptions == rhs.descriptions) &&
               (lhs.name == rhs.name) &&
               (lhs.qty == rhs.qty)
    }
}
