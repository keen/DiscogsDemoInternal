//
//  MasterViewController.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/9/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import UIKit
import Siesta

class MasterViewController: UITableViewController, ResourceObserver {

    var detailViewController: DetailViewController? = nil
    var wantedItemsSource = [DiscogsAlbumSummary]()
    var latestPaginationInformation: Pagination?
    var lookingForMorePages: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        // This makes the MasterViewController an observer of the resources at discogs API endpoint "/users/keehun/wants/"
        // The both discogs and wantedItems are set up in the DiscogsAPI.swift for cleanliness
        discogs.wantedItems.addObserver(self)
        discogs.wantedItems.loadIfNeeded()
        
        
        // Ask Autolayout to automatically size our table rows
        // A good approximation happens to be a square because of
        //  the layout we're looking for
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = self.tableView.bounds.width
        
        // Set up the activity indicator for our infinity scroll.
        // This can just live in the footer until we've reached the last page as new content will push this
        //   footer further down. On the last page, we will remove the footer view. The container view is 20
        //   points taller than the combined spinner and label because we want some padding below the label.
        let tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 88))
        tableFooterView.autoresizesSubviews = true
        
        let infinityScrollSpinner = UIActivityIndicatorView(activityIndicatorStyle: .white)
        infinityScrollSpinner.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin]
        infinityScrollSpinner.translatesAutoresizingMaskIntoConstraints = true
        infinityScrollSpinner.startAnimating()
        infinityScrollSpinner.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: 44)
        tableFooterView.addSubview(infinityScrollSpinner)
        
        let loadingMessage = UILabel(frame: CGRect(x: 0, y: 44, width: self.tableView.frame.width, height: 24))
        loadingMessage.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        loadingMessage.translatesAutoresizingMaskIntoConstraints = true
        loadingMessage.font = UIFont(name: "Avenir-Book", size: 14)
        loadingMessage.textColor = UIColor.white
        loadingMessage.textAlignment = .center
        loadingMessage.numberOfLines = 1
        loadingMessage.text = "Loading Albums"
        tableFooterView.addSubview(loadingMessage)
        
        self.tableView.tableFooterView = tableFooterView
        
        
        // Boilerplate for iPad
        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if discogs.consumerKey == "KEY", discogs.consumerSecret == "SECRET" {
            let alert = UIAlertController(title: "No API Key/Secret", message: "Nothing will appear until the API key/secret is entered into environment variables in Xcode's Scheme Editor or in DiscogsAPI.swift", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
    }
    
    // MARK: - Discogs API Observer
    func resourceChanged(_ resource: Resource, event: ResourceEvent) {
        if let response = resource.latestData?.content as? DiscogsWantsResponse {
            if let wantedItems = response.wantedItems {
                wantedItemsSource = wantedItems
                if let table = self.tableView {
                    table.reloadData()
                }
                
                // This new pagination information will be used the next time the album list reaches the end to load
                //   the next page.
                if let pagination = response.pagination {
                    latestPaginationInformation = pagination
                }
            }
        }
    }
    
    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            self.title = "Discogs"
            if let indexPath = tableView.indexPathForSelectedRow {
                if wantedItemsSource[indexPath.row].basicInformation != nil {
                    let albumID = wantedItemsSource[indexPath.row].basicInformation?.id
                    let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                    controller.resourceID = albumID
                    controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                    controller.navigationItem.leftItemsSupplementBackButton = true
                }
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return wantedItemsSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:AlbumArtTableCell = tableView.dequeueReusableCell(withIdentifier: "AlbumArtCell", for: indexPath) as! AlbumArtTableCell

        let album = wantedItemsSource[indexPath.row]
        if let albumBasicInformation = album.basicInformation {
            cell.albumTitle?.text = albumBasicInformation.title
            if let artists = albumBasicInformation.artists {
                cell.albumArtist?.text = artists.reduce("", { $0 == "" ? $1.name ?? "" : "\($0), \($1.name ?? "")" })
            }
            
            // When there is no album art, this particular API endpoint returns a spacer gif instead of nothing.
            if albumBasicInformation.coverImage != "https://img.discogs.com/images/spacer.gif" {
                cell.albumCoverView?.imageURL = albumBasicInformation.coverImage
            } else {
                cell.albumCoverView?.imageURL = "https://s.discogs.com/images/default-release.png"
            }
        }
        
        // For reasons unknown, setting highlightedTextColor in Interface Builder doesn't work.
        cell.albumTitle?.highlightedTextColor = UIColor.black
        cell.albumArtist?.highlightedTextColor = UIColor.black

        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        // Infinity Scroll -- Check if we're at the end of the table
        if (indexPath.row == wantedItemsSource.count - 1) {
            
            // Check if we've already reached the end
            if lookingForMorePages {
                if let urls = latestPaginationInformation?.urls {
                    
                    // Retrieve the next page URL
                    if let next = urls.next {
                        
                        // Request parameters based on "next" link of pagination.
                        // Siesta will automatically html-encode the ? required for params. Must do this without hacking away on Siesta
                        let perPage = getQueryStringParameter(url: next, param: "per_page")
                        let page    = getQueryStringParameter(url: next, param: "page")
                        let nextPageResource: Resource = discogs.wantedItems.withParam("per_page", perPage).withParam("page",page)
                       
                        // Now, instead of using observer/delegate methods to add the data as we did initially, do it as it comes in.
                        nextPageResource.loadIfNeeded()?.onSuccess({ (response) in
                            if let content = response.content as? DiscogsWantsResponse {
                                if let newWantedItems = content.wantedItems {
                                    self.wantedItemsSource.append(contentsOf: newWantedItems)
                                    self.latestPaginationInformation = content.pagination
                                    self.tableView.reloadData()
                                }
                            }
                        })
                    } else {
                        // There are no more pages to load (when "next" link is empty)
                        self.lookingForMorePages = false
                        self.tableView.tableFooterView = nil
                    }
                }
            }
        }
    }
    
    // MARK: - Handling Rotation
    
    // When the album artwork cell changes orientation from landscape to portrait, the album title label
    //   goes from very wide to thin. During this layout change, it fails to reflow its text to multiple
    //   lines. Combined with the UILabel subclass override code borrowed from https://stackoverflow.com/a/23497077,
    //   we must also reassign preferredMaxLayoutWidth as well as update its constraints. The code
    //   found on StackOverflow itself does not solve this bug by Apple.
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        if let visibleRows = self.tableView.indexPathsForVisibleRows {
            for indexPath in visibleRows {
                if let cell:AlbumArtTableCell = self.tableView.cellForRow(at: indexPath) as? AlbumArtTableCell {
                    cell.albumTitle.preferredMaxLayoutWidth = cell.albumTitle.bounds.size.width
                    cell.albumTitle.updateConstraints()
                }
            }
        }
    }
}

