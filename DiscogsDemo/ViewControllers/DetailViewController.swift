//
//  DetailViewController.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/9/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import UIKit
import Siesta
import Cosmos

class DetailViewController: UIViewController, ResourceObserver {

    @IBOutlet weak var albumCoverImageView: RemoteImageView!
    @IBOutlet weak var albumTitleLabel: UILabel!
    @IBOutlet weak var albumRatingView: CosmosView!
    @IBOutlet weak var albumTracksTableView: UITableView!
    @IBOutlet weak var albumCreditsTableView: UITableView!
    @IBOutlet weak var albumMetadataTableView: UITableView!
    @IBOutlet weak var albumNotesHeader: UILabel!
    @IBOutlet weak var albumNotes: UILabel!
    @IBOutlet weak var loadingIndicator: UIView!
    @IBOutlet weak var loadingIndicatorSpinner: UIActivityIndicatorView!
    @IBOutlet weak var selectAlbumLabel: UILabel!
    
    let tracksTableViewController: DataTableViewController = DataTableViewController()
    let creditsTableViewController: DataTableViewController = DataTableViewController(firstColumnWidth: 150.0)
    let metadataTableViewController: DataTableViewController = DataTableViewController(firstColumnWidth: 150.0)

    func configureView() {
        self.loadingIndicatorSpinner.alpha = 0
        self.loadingIndicator.alpha = 1
        
        // Update the user interface for the detail item.
        if let albumID = resourceID {
            self.loadingIndicatorSpinner.alpha = 1
            selectAlbumLabel.isHidden = true
            discogs.album(albumID).addObserver(self)
            discogs.album(albumID).loadIfNeeded()
        }

        albumCoverImageView.layer.shadowColor = UIColor.black.cgColor
        albumCoverImageView.layer.shadowOffset = CGSize(width: 3, height: 3)
        albumCoverImageView.layer.shadowOpacity = 1
        albumCoverImageView.layer.shadowRadius = 5.0
        albumCoverImageView.clipsToBounds = false
        
        tracksTableViewController.tableView = albumTracksTableView
        creditsTableViewController.tableView = albumCreditsTableView
        metadataTableViewController.tableView = albumMetadataTableView
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = " "
        // Do any additional setup after loading the view, typically from a nib.
        configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    var resourceID: Int?

    // MARK: - Discogs API Observer
    
    func resourceChanged(_ resource: Resource, event: ResourceEvent) {
        if let response = resource.latestData?.content as? DiscogsAlbumResource {
            if let imageURL = response.images?.first?.resourceURL {
                albumCoverImageView?.imageURL = imageURL
            } else {
                albumCoverImageView?.imageURL = "https://s.discogs.com/images/default-release.png"
            }
            
            if let title = response.title?.trimmingCharacters(in: .whitespacesAndNewlines) {
                self.title = title
                albumTitleLabel.text = title
            }
            
            var renderedTrackList: [Section] = [Section]()
            if let tracklist = response.tracklist {
                renderedTrackList = renderList(forTracks: tracklist)
            }
            
            var renderedCreditsList: [Section] = [Section]()
            if let artists = response.artists {
                renderedCreditsList = renderList(forArtists: artists)
            }
            
            let renderedMetadataList = renderList(forMetadataInResponse: response)
            
            if let note = response.notes {
                albumNotesHeader.isHidden = false
                albumNotes.text = note
            } else {
                albumNotes.isHidden = true
                albumNotesHeader.isHidden = true
            }

            CATransaction.begin()
            CATransaction.setCompletionBlock {
                UIView.animate(withDuration: 0.3, animations: {
                    self.loadingIndicator.alpha = 0
                })
            }
            
            if let rating = response.community?.rating {
                if let count = rating.count {
                    if count > 0 {
                        self.albumRatingView?.isHidden = false
                        self.albumRatingView?.rating = Double(rating.average ?? 0)
                        self.albumRatingView?.text = String("(\(rating.count ?? 0))")
                    } else {
                        self.albumRatingView?.isHidden = true
                    }
                }
            }
            
            self.tracksTableViewController.dataForDisplay = renderedTrackList
            self.metadataTableViewController.dataForDisplay = renderedMetadataList
            self.creditsTableViewController.dataForDisplay = renderedCreditsList
            self.view.setNeedsLayout()
            
            CATransaction.commit()
        }
    }
}

