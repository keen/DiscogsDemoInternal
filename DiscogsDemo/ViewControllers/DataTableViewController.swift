//
//  DataTableViewController.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/10/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import UIKit

struct Section {
    var sectionTitle: String
    var allData: [Data]
}

struct Data {
    var heading: String
    var data: String
}

class DataTableViewController: UITableViewController {

    var firstColumnWidth: CGFloat = 50.0
    var tableHeightConstraint:NSLayoutConstraint?
    
    // Each tuple represents a row of information
    var dataForDisplay: [Section]? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    convenience init(firstColumnWidth: CGFloat) {
        self.init(style: .plain)
        self.firstColumnWidth = firstColumnWidth
    }
    
    override func viewWillLayoutSubviews() {
        super.updateViewConstraints()
        
        if tableHeightConstraint == nil {
            let filteredConstraints = self.tableView.constraints.filter { $0.identifier == "heightConstraint" }
            if let heightConstraint = filteredConstraints.first {
                tableHeightConstraint = heightConstraint
            }
        }
        
        if let constraint = tableHeightConstraint {
            constraint.constant = self.tableView.contentSize.height
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        if let sections = dataForDisplay {
            return sections.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = dataForDisplay {
            return sections[section].allData.count
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "Avenir-Bold", size: 14.0)
        header.textLabel?.textAlignment = .center
        header.textLabel?.adjustsFontSizeToFitWidth = true
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // DataTableView has already registered the DataCell class with this Identifier
        let cell:DataTableViewCell = tableView.dequeueReusableCell(withIdentifier: DataTableViewCell.identifier,
                                                                   for: indexPath) as! DataTableViewCell
        cell.configureCell(withFirstColumnWidth: firstColumnWidth)
        if let sections = dataForDisplay {
            let heading = sections[indexPath.section].allData[indexPath.row].heading
            let data = sections[indexPath.section].allData[indexPath.row].data
            
            cell.dataTitleLabel?.text = heading
            cell.dataContentLabel?.text = data
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let sections = dataForDisplay {
            return sections[section].sectionTitle
        } else {
            return ""
        }
    }
}
