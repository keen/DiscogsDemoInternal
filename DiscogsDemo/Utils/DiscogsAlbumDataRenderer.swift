//
//  DiscogsAlbumDataRenderer.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/12/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import Foundation

/**
 This function will take a list of tracks and return a generic section/row data that can easily be used for UITableView
 
 @param trackList An array of Track objects
 
 @return An array of Sections which contain row data within each section
 */
func renderList(forTracks trackList: [Track]) -> [Section] {
    // Accumulator for, eventually, all tracks
    var renderedTrackList: [Section] = [Section]()
    
    // Temporary accumulator per each section
    var currentSection: Section?
    
    // I am sure someone could figure out how to write one reduce closure to achieve the same thing...
    //      but at least this is infinitely more readable!
    //
    // Iterate through each top-level track. These track objects can either be:
    //      * index -- this means it's serving as headers for its subtracks
    //      * track -- this is an actual track with NO subtracks
    // Because we're using table view section headers to achieve "index" tracks,
    //      we have to translate these two slightly different structures ourselves.
    
    for track: Track in trackList {
        if track.type == "index" || track.type == "heading" {
            
            // The API may give us something like this:
            //  header-type track #0
            //  track-type track #1
            //  track-type track #2
            //  index-type track #3
            //      track-type subtrack #3A
            //      track-type subtrack #3B
            //      track-type subtrack #3C
            //  track-type track #4
            //
            //  And in this case, those tracks need to appear exactly in that order, as displayed
            //  Therefore, once we get to #3, #1 and #2 need to be pushed inte their own sections
            //    before moving onto #3.
            //
            // Although the data structures currently support recursion, there is no reason to support
            //   recursive flattening of the structure as it doesn't make sense for the data in the real
            //   world to be recursive. The API is probably not even designed to be able to output JSON
            //   recursively.
            
            // If currentSection is not nil (meaning, there are pre-existing tracks on there), then add it to
            //   the structure
            if let section = currentSection {
                renderedTrackList.append(section)
            }
            
            // Init new Section with the header text or blank
            currentSection = Section(sectionTitle: track.title?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                                     allData: [])
            
            // Ideally, index-type tracks should have subtracks, but
            //   check again just in case
            if let subtracks = track.subtracks {
                for subtrack in subtracks {
                    
                    // We finally got the data we're looking for. Add it to our accumulator array
                    currentSection?.allData.append(Data(heading: subtrack.position?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                                                        data: subtrack.title?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""))
                }
            }
            
            // We've gone through all index-type top-level tracks
            if let section = currentSection {
                renderedTrackList.append(section)
            }
            
            // Then reset currentSection in case we have another index-type
            //   or a header-type track
            currentSection = nil
            
        } else {
            
            if currentSection == nil {
                currentSection = Section(sectionTitle: "", allData: [])
            }
            
            // We finally got the track data we're looking for. Add it to our accumulator array
            currentSection?.allData.append(Data(heading: track.position?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                                                data: track.title?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""))
        }
    }
    // Add the last section to our stack.
    if let section = currentSection {
        renderedTrackList.append(section)
    }
    
    return renderedTrackList
}

/**
 This function will take a list of artists and their roles and return a generic section/row data that can easily be used for UITableView
 
 @param artistsList An array of Artist objects
 
 @return An array of Sections which contain row data within each section
 */
func renderList(forArtists artistsList: [Artist]) -> [Section] {
    var renderedCreditsList: [Section] = [Section]()
    
    // No section title necessary
    var section = Section(sectionTitle: "", allData: [])
    
    for artist:Artist in artistsList {
        section.allData.append(Data(heading: artist.role?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "",
                                    data: artist.name?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""))
        
    }
    
    renderedCreditsList.append(section)
    
    return renderedCreditsList
}

/**
 This function will combine various properties from the DiscogsAlbumResource struct and return a generic section/row data that can easily be used for UITableView. DiscogsAPI does not return a single Metadata field, so we have to combine various fields to weave our own.
 
 @param response A DiscogsAlbumResource object containing all the necessary data
 
 @return An array of Sections which contain row data within each section
 */
func renderList(forMetadataInResponse response: DiscogsAlbumResource) -> [Section] {
    var renderedMetadataList: [Section] = [Section]()
    var metadataSection = Section(sectionTitle: "", allData: [])
    
    if let country = response.country {
        metadataSection.allData.append(Data(heading: "Country", data: country))
    }
    
    if let formats = response.formats {
        for format in formats {
            var formatsString: String = ""
            
            var formatLabel:String = (formats.count == 1) ? "Format" : "Formats"
            
            if format != formats.first {
                formatLabel = ""
            }
            
            if let quantityString = format.qty {
                if let quantityInt = Int(quantityString) {
                    if quantityInt > 1 {
                        formatsString.append("\(quantityString) x ")
                    }
                }
            }
            
            if let formatName = format.name {
                formatsString.append(formatName)
            }
            
            if let formatDescriptions = format.descriptions {
                for description in formatDescriptions {
                    formatsString.append(", \(description)")
                }
            }
            
            metadataSection.allData.append(Data(heading: formatLabel, data: formatsString))
        }
    }
    
    if let genres = response.genres {
        metadataSection.allData.append(Data(heading: "Genres", data: genres.reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })))
    }
    
    if let identifiers = response.identifiers {
        for identifier in identifiers {
            if let type = identifier.type, let value = identifier.value {
                metadataSection.allData.append(Data(heading: type, data: value))
            }
        }
    }
    
    if let releasedFormatted = response.releasedFormatted {
        metadataSection.allData.append(Data(heading: "Released", data: releasedFormatted))
    }
    
    if let styles = response.styles {
        metadataSection.allData.append(Data(heading: "Styles", data: styles.reduce("", { $0 == "" ? $1 : $0 + ", " + $1 })))
    }
    
    if let year = response.year {
        metadataSection.allData.append(Data(heading: "Year", data: String(year)))
    }
    
    renderedMetadataList.append(metadataSection)
    
    return renderedMetadataList
}
