//
//  URL+ValueOfParam.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/12/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//
//  Borrowed from: https://stackoverflow.com/questions/41421686/get-the-value-of-url-parameters
//


import Foundation

func getQueryStringParameter(url: String, param: String) -> String? {
    guard let url = URLComponents(string: url) else { return nil }
    return url.queryItems?.first(where: { $0.name == param })?.value
}
