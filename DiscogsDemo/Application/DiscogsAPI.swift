//
//  DiscogsAPI.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/9/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import Foundation
import Siesta

/**
 Discogs.com API
 */

class DiscogsAPI: Service {
    
    // In production-code, this should be replaced by using OAuth/Token flow
    let consumerKey = ProcessInfo.processInfo.environment["consumer_key"] ?? "KEY"
    let consumerSecret = ProcessInfo.processInfo.environment["consumer_secret"] ?? "SECRET"

    // Hard-coded user name for the purposes of this demonstration, but this could easily be made dynamic later on.
    // The reason for hard-coding is to curate the experience you will have upon seeing my demo and to make sure
    //     that the albums chosen are appropriate to showing you all the features of this demo application.
    let userName = "keehun"

    init() {
        super.init(baseURL: "https://api.discogs.com", standardTransformers: [.text, .image])

        // To enable the logging firehose for debugging:
        // Siesta.LogCategory.enabled = LogCategory.all
        
        // Global default headers
        configure {
            $0.headers["User-Agent"] = "KeehunDiscogSwiftDemo/0.1 +http://keehun.com"
            
            // The consumer secret and key are set in XCode's Environment Variables in the Scheme Editor
            $0.headers["Authorization"] = "Discogs key=\(self.consumerKey), secret=\(self.consumerSecret)"
        }

        let jsonDecoder = JSONDecoder()
        configureTransformer("/users/*/wants") { (response) in
            try jsonDecoder.decode(DiscogsWantsResponse.self, from: response.content)
        }
        configureTransformer("/releases/*") { (response) in
            try jsonDecoder.decode(DiscogsAlbumResource.self, from: response.content)
        }
    }

    var wantedItems: Resource { return resource("/users").child(userName).child("wants") }
    
    func album(_ resourceID: Int) -> Resource {
        return discogs
            .resource("/releases")
            .child(String(resourceID))
    }
}

let discogs = DiscogsAPI()
