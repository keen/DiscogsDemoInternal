//
//  DataTableViewCell.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/10/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import UIKit

class DataTableViewCell: UITableViewCell {
    static let identifier: String = "DataCell"
    
    var firstColumnWidth: CGFloat = 50.0
    var dataTitleLabel: UILabel!
    var dataContentLabel: UILabel!

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        dataTitleLabel = UILabel()
        dataTitleLabel.font = UIFont(name: "Avenir-Bold", size: 14)
        dataTitleLabel.textColor = UIColor.black
        dataTitleLabel.textAlignment = .left
        dataTitleLabel.numberOfLines = 0
        
        dataContentLabel = UILabel()
        dataContentLabel.font = UIFont(name: "Avenir-Bold", size: 14)
        dataContentLabel.textColor = UIColor.black
        dataContentLabel.textAlignment = .left
        dataContentLabel.numberOfLines = 0
        
        contentView.addSubview(dataTitleLabel)
        contentView.addSubview(dataContentLabel)
    }
    
    func configureCell(withFirstColumnWidth columnWidth: CGFloat) {
        dataTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        dataTitleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor).isActive = true
        dataTitleLabel.topAnchor.constraint(equalTo: dataContentLabel.topAnchor).isActive = true
        dataTitleLabel.bottomAnchor.constraint(equalTo: dataContentLabel.bottomAnchor).isActive = true
        dataTitleLabel.widthAnchor.constraint(equalToConstant: columnWidth).isActive = true
        dataTitleLabel.heightAnchor.constraint(equalTo: dataContentLabel.heightAnchor).isActive = true
        
        dataContentLabel.translatesAutoresizingMaskIntoConstraints = false
        dataContentLabel.leadingAnchor.constraint(equalTo: dataTitleLabel.trailingAnchor).isActive = true
        dataContentLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor).isActive = true
        dataContentLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10.0).isActive = true
        dataContentLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10.0).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
}
