//
//  AlbumArtTableCell.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/10/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import UIKit
import Siesta

class AlbumArtTableCell: UITableViewCell {
    @IBOutlet weak var albumCoverView: RemoteImageView!
    @IBOutlet weak var albumTitle: UILabel!
    @IBOutlet weak var albumArtist: UILabel!
}
