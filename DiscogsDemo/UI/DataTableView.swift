//
//  DataTableView.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/10/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import UIKit

class DataTableView: UITableView {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.separatorStyle = .singleLine
        self.separatorColor = UIColor.lightGray
        self.separatorInset = UIEdgeInsets.zero
        self.isScrollEnabled = false
        self.allowsSelection = false
        self.estimatedRowHeight = 44
        self.rowHeight = UITableViewAutomaticDimension
        self.register(DataTableViewCell.self, forCellReuseIdentifier: DataTableViewCell.identifier)
    }
}

