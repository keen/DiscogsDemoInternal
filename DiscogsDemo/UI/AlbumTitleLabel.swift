//
//  AlbumTitleLabel.swift
//  DiscogsDemo
//
//  Created by Keehun Nam on 5/15/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//
//
// This code is borrowed from: https://stackoverflow.com/a/23497077
// Instead of globally extending UILabel, we subclass and only assign
//   the affected labels to this subclass as other labels not in a tableview
//   cell reflow perfectly in conjunction with AutoLayout
// This issue is not yet 100% resolved. Overcoming AutoLayout bugs 

import Foundation
import UIKit

class AlbumTitleLabel: UILabel {
    override open func layoutSubviews() {
        super.layoutSubviews()
        self.preferredMaxLayoutWidth = self.bounds.width
        super.layoutSubviews()
    }
    
    override open var bounds: CGRect {
        didSet {
            self.preferredMaxLayoutWidth = self.bounds.width
        }
    }

    override open var frame: CGRect {
        didSet {
            self.preferredMaxLayoutWidth = self.frame.width
        }
    }
}
