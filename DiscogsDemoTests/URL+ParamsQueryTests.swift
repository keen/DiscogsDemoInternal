//
//  URL+ParamsQueryTests.swift
//  DiscogsDemoTests
//
//  Created by Keehun Nam on 5/12/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import XCTest

class URL_ParamsQueryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }

    func testURLGetQueryParam() {
        let testURL: String = "https://api.discogs.com/users/keehun/wants?per_page=50&page=1"
        
        let perPage = getQueryStringParameter(url: testURL, param: "per_page")
        let page    = getQueryStringParameter(url: testURL, param: "page")
        
        XCTAssert(perPage == "50")
        XCTAssert(page == "1")
    }
}
