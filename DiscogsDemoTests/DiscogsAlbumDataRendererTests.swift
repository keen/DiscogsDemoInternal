//
//  DiscogsAlbumDataRendererTests.swift
//  DiscogsDemoTests
//
//  Created by Keehun Nam on 5/12/18.
//  Copyright © 2018 Keehun Nam. All rights reserved.
//

import XCTest

class DiscogsAlbumDataRendererTests: XCTestCase {
    
    var decodedAlbum: DiscogsAlbumResource?
    
    override func setUp() {
        super.setUp()
        XCTAssertNoThrow(try setupJSON())
    }
    
    func setupJSON() throws {
        let bundle = Bundle(for: type(of: self))
        guard let url = bundle.url(forResource: "exampleAlbum", withExtension: "json") else {
            XCTFail("Missing file: exampleAlbum.json")
            return
        }
        
        let jsonDecoder:JSONDecoder = JSONDecoder()
        
        if let album = try jsonDecoder.decode(DiscogsAlbumResource.self, from: Foundation.Data(contentsOf: url)) as DiscogsAlbumResource? {
            self.decodedAlbum = album
        } else {
            XCTFail("Failed to setup example JSON")
        }
    }
    
    // Test of the data flattener that converts the track information to generic TableView section/row information
    func testRenderTracklist() {
        if let tracklist = decodedAlbum?.tracklist {
            let renderedTrackList: [Section] = renderList(forTracks: tracklist)
            XCTAssertEqual(renderedTrackList.count, 2)
            XCTAssertEqual(renderedTrackList[0].sectionTitle, String("Symphony No. 1 In D Major \u{201c}Titan\u{201d}"))
            XCTAssertEqual(renderedTrackList[0].allData.count, 1)
            XCTAssertEqual(renderedTrackList[0].allData[0].heading, "1-1")
            XCTAssertEqual(renderedTrackList[0].allData[0].data, "I Langsam. Schleppend. Wie Ein Naturlaut. Im Anfang Sehr Gem\u{00e4}chlich")
            
            XCTAssertEqual(renderedTrackList[1].sectionTitle, String("Symphony No. 2 In C Minor \u{201c}Resurrection\u{201d}"))
            XCTAssertEqual(renderedTrackList[1].allData.count, 1)
            XCTAssertEqual(renderedTrackList[1].allData[0].heading, "2-1")
            XCTAssertEqual(renderedTrackList[1].allData[0].data, "I Allegro Maestoso. Mit DurchausI Ernstem Und Feierlichem Ausdruck")
            
        } else {
            XCTFail("Tracklist field was nil")
        }
    }
    
    func testAlbumMetadata() {
        XCTAssertEqual(decodedAlbum?.id, 3408094)
        XCTAssertEqual(decodedAlbum?.title, "Symphonies 1-10")
        XCTAssertEqual(decodedAlbum?.year, 1974)
        XCTAssertEqual(decodedAlbum?.releasedFormatted, "1974")
        XCTAssertEqual(decodedAlbum?.released, "1974")
        XCTAssertEqual(decodedAlbum?.country, "Germany")
        XCTAssertEqual(decodedAlbum?.dateAdded, "2012-02-14T08:27:09-08:00")
        XCTAssertEqual(decodedAlbum?.notes, "Some Note")
        XCTAssertEqual(decodedAlbum?.uri, "https://www.discogs.com/Gustav-Mahler-Leonard-Bernstein-Symphonies-1-10/release/3408094")
    }
    
    func testAlbumArtists() {
        // May be confusing as we're actually interpreting "extraartists" field as "artists" instead
        //   of the literal "artists" field
        if let artists = decodedAlbum?.artists {
            XCTAssertEqual(artists.count, 3)
            let artist1 = artists[0]
            XCTAssertEqual(artist1.name, "Gustav Mahler")
            XCTAssertEqual(artist1.role, "Composed By")
            XCTAssertEqual(artist1.resourceURL, "https://api.discogs.com/artists/239236")
            XCTAssertEqual(artist1.id, 239236)
            
            let artist2 = artists[1]
            XCTAssertEqual(artist2.name, "Leonard Bernstein")
            XCTAssertEqual(artist2.role, "Conductor")
            XCTAssertEqual(artist2.resourceURL, "https://api.discogs.com/artists/299702")
            XCTAssertEqual(artist2.id, 299702)
            
            let artist3 = artists[2]
            XCTAssertEqual(artist3.name, "The New York Philharmonic Orchestra")
            XCTAssertEqual(artist3.role, "Orchestra")
            XCTAssertEqual(artist3.resourceURL, "https://api.discogs.com/artists/388185")
            XCTAssertEqual(artist3.id, 388185)
        } else {
            XCTFail("Artists field was nil")
        }
    }
    
    func testAlbumTracklist() {
        if let tracks = decodedAlbum?.tracklist {
            XCTAssertEqual(tracks.count, 2)
            
            let track1 = tracks[0]
            XCTAssertEqual(track1.title, "Symphony No. 1 In D Major \u{201c}Titan\u{201d}")
            XCTAssertEqual(track1.type, "index")

            XCTAssertEqual(track1.subtracks?.count, 1)
            let subtrack1 = track1.subtracks?[0]
            XCTAssertEqual(subtrack1?.title, "I Langsam. Schleppend. Wie Ein Naturlaut. Im Anfang Sehr Gem\u{00e4}chlich")
            XCTAssertEqual(subtrack1?.position, "1-1")
            
            let track2 = tracks[1]
            XCTAssertEqual(track2.title, "Symphony No. 2 In C Minor \u{201c}Resurrection\u{201d}")
            XCTAssertEqual(track2.type, "index")
            
            XCTAssertEqual(track2.subtracks?.count, 1)
            let subtrack2 = track2.subtracks?[0]
            XCTAssertEqual(subtrack2?.title, "I Allegro Maestoso. Mit DurchausI Ernstem Und Feierlichem Ausdruck")
            XCTAssertEqual(subtrack2?.position, "2-1")
        } else {
            XCTFail("Tracklist field was nil")
        }
    }
    
    func testAlbumImages() {
        if let images = decodedAlbum?.images {
            XCTAssertEqual(images.count, 1)
            XCTAssertEqual(images[0].height, 597)
            XCTAssertEqual(images[0].width, 600)
            XCTAssertEqual(images[0].uri, "https://img.discogs.com/VrzqobwfWK9OFA1B0J_dJqiLANI=/fit-in/600x597/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-3408094-1329236704.jpeg.jpg")
            XCTAssertEqual(images[0].resourceURL, "https://img.discogs.com/VrzqobwfWK9OFA1B0J_dJqiLANI=/fit-in/600x597/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-3408094-1329236704.jpeg.jpg")
            XCTAssertEqual(images[0].type, "secondary")
            XCTAssertEqual(images[0].uri150, "https://img.discogs.com/Dj9LrFEIhvkEpio6Eyj-H-S3esY=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb():quality(40)/discogs-images/R-3408094-1329236704.jpeg.jpg")
        } else {
            XCTFail("Images field was nil")
        }
    }
    
    func testAlbumGenres() {
        if let genres = decodedAlbum?.genres {
            XCTAssertEqual(genres.count, 1)
            XCTAssertEqual(genres[0], "Classical")
        } else {
            XCTFail("Genres field was nil")
        }
    }
    
    func testAlbumStyles() {
        if let styles = decodedAlbum?.styles {
            XCTAssertEqual(styles.count, 1)
            XCTAssertEqual(styles[0], "Romantic")
        } else {
            XCTFail("Styles field was nil")
        }
    }
    
    func testAlbumFormats() {
        if let formats = decodedAlbum?.formats {
            XCTAssertEqual(formats.count, 2)
            XCTAssertEqual(formats[0].descriptions!.count, 1)
            XCTAssertEqual(formats[0].descriptions![0], "LP")
            XCTAssertEqual(formats[0].name , "Vinyl")
            XCTAssertEqual(formats[0].qty , "15")
            
            XCTAssertEqual(formats[1].name, "Box Set")
            XCTAssertEqual(formats[1].qty, "1")
        } else {
            XCTFail("Formats field was nil")
        }
        
        // Test if formats custom equals function works
        let format1A: Format = Format(withDescriptions: ["Description1", "Description2"],
                                     quantity: "12",
                                     andName: "FormatName1")
        let format1B: Format = Format(withDescriptions: ["Description1", "Description2"],
                                     quantity: "12",
                                     andName: "FormatName1")
        let formatWithWrongQuantity: Format = Format(withDescriptions: ["Description1", "Description2"],
                                     quantity: "10",
                                     andName: "FormatName1")
        let formatWithWrongDescriptions: Format = Format(withDescriptions: ["Description2", "Description3"], quantity: "12", andName: "FormatName1")
        let formatWithSameDescriptionsButWrongOrder: Format = Format(withDescriptions: ["Description2", "Description1"], quantity: "12", andName: "FormatName1")
        let formatWithWrongName: Format = Format(withDescriptions: ["Description1", "Description2"], quantity: "12", andName: "FormatName2")
        
        XCTAssert(format1A == format1B, "Two same Format objects should be evaluated as equal")
        XCTAssertFalse(format1A == formatWithWrongQuantity, "Format with a different quantity should not be evaluated as equal")
        XCTAssertFalse(format1A == formatWithWrongDescriptions, "Format with a different descriptions should not be evaluated as equal")
        XCTAssertFalse(format1A == formatWithSameDescriptionsButWrongOrder, "Format with wrong order of the same descriptions should not be evaluated as equal")
        XCTAssertFalse(format1A == formatWithWrongName, "Format with a different name should not be evaluated as equal")
    }
    
    func testAlbumCommunity() {
        if let community = decodedAlbum?.community {
            XCTAssertEqual(community.status, "Accepted")
            XCTAssertEqual(community.rating?.count, 9)
            XCTAssertEqual(community.rating?.average, 5.0)
            XCTAssertEqual(community.have, 70)
            XCTAssertEqual(community.want, 34)
            XCTAssertEqual(community.dataQuality, "Needs Vote")
        } else {
            XCTFail("Community field was nil")
        }
    }
    
    func testAlbumIdentifiers() {
        if let identifiers = decodedAlbum?.identifiers {
            XCTAssertEqual(identifiers.count, 2)
            XCTAssertEqual(identifiers[0].type, "Barcode")
            XCTAssertEqual(identifiers[0].value, "886979433328")
            XCTAssertEqual(identifiers[1].type, "Label Code")
            XCTAssertEqual(identifiers[1].value, "LC 06868")
        } else {
            XCTFail("Identifiers field was nil")
        }
    }
}
