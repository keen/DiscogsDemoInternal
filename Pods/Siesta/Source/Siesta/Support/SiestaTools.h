//
//  SiestaTools.h
//  Siesta
//
//  Created by Paul on 2017/11/22.
//  Copyright © 2016 Bust Out Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for SiestaTools.
FOUNDATION_EXPORT double SiestaToolsVersionNumber;

//! Project version string for SiestaTools.
FOUNDATION_EXPORT const unsigned char SiestaToolsVersionString[];

